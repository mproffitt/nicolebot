from skpy import SkypeEventLoop, SkypeMessageEvent, SkypeNewMessageEvent, SkypeTextMsg, SkypeEditMessageEvent


class NicoleBot(SkypeEventLoop):
    def __init__(self,
                 username,
                 password,
                 chat_target,
                 user_target,
                 default_message,
                 message_transformer=(lambda message: message),
                 token_path=None,
                 debug=False):
        super(NicoleBot, self).__init__(username, password, token_path)
        self.chat_target = chat_target
        self.user_target = user_target
        self.default_message = default_message
        self.message_transformer = message_transformer
        self.debug = debug
        self.target_message_cache = {}

    def onEvent(self, event):
        if self.debug:
            print(event)
        if isinstance(event, SkypeMessageEvent):
            if self.debug:
                print(event.msg)
            if event.msg.chat.id == self.chat_target:
                print('Message from target chat...')
                if event.msg.user.id == self.user_target:
                    print('  Message from target user...')
                    if isinstance(event, SkypeNewMessageEvent):
                        if isinstance(event.msg, SkypeTextMsg):
                            print('    Saving target message.')
                            self.target_message_cache[event.msg.clientId] = event.msg.plain
                        else:
                            print('    Unknown message type.')
                    if isinstance(event, SkypeEditMessageEvent) and event.msg.content is None:
                        print('    Target message deleted!')
                        if event.msg.clientId in self.target_message_cache:
                            print('      And I have it!')
                            original_message = self.target_message_cache[event.msg.clientId]
                        else:
                            print("      But I don't have it.")
                            original_message = self.default_message
                        transformed_message = self.message_transformer(original_message)
                        print('      Sending:', transformed_message)
                        event.msg.chat.sendMsg(transformed_message)
                else: # Non-target user
                    print('  Message from non-target user.')
            else: # Non-target chat
                print('Message from non-target chat.')
            print()
        else: # Non-message event
            if self.debug:
                print()
