#!/usr/bin/env python3

from nicolebot import NicoleBot
import config

print('Connecting...')
nb = NicoleBot(config.username,
               config.password,
               config.chat_target,
               config.user_target,
               config.default_message,
               config.message_transformer,
               config.token_path,
               config.debug)
print('Connected.')
print('Entering event loop...')
print()
nb.loop()
